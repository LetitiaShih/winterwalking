using UnityEngine;

class MyScreenInputProvider : MyInputProvider
{

    private Command _command = Command.NoAction;

    public void UpdateInput()
    {
        Touch touch = Input.GetTouch(0);
        if (touch.position.x < Screen.width / 2)
        {
            //tap screen left: crouch
            _command = Command.Crouch;
        }
        else if (touch.position.x >= Screen.width / 2)
        {
            //tap screen right: jump or fly
            if (touch.phase == TouchPhase.Began)
            {
                _command = Command.Jump;
            }
            else if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
            {
                _command = Command.HoldJump;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                _command = Command.NoAction;
            }
        }
        else
        {
            Debug.Log("Unexpected touch event happened");
        }
    }

    public Command GetCommand()
    {
        return _command;
    }

}