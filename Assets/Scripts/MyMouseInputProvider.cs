using UnityEngine;

class MyMouseInputProvider : MyInputProvider
{
    private static int LEFT_MOUSE_BUTTON = 0;
    private static int RIGHT_MOUSE_BUTTON = 1;

    private bool rightMouseClicked;

    private Command _command = Command.NoAction;

    public void UpdateInput()
    {
        // TODO:
        if (Input.GetMouseButton(RIGHT_MOUSE_BUTTON) || Input.GetMouseButtonDown(RIGHT_MOUSE_BUTTON) || Input.GetMouseButtonUp(RIGHT_MOUSE_BUTTON))
        {
            _command = Command.Crouch;
        }
        else if (Input.GetMouseButtonDown(LEFT_MOUSE_BUTTON))
        {
            _command = Command.Jump;
        }
        else if (Input.GetMouseButton(LEFT_MOUSE_BUTTON))
        {
            _command = Command.HoldJump;
        }
        else
        {
            _command = Command.NoAction;
        }
    }

    public Command GetCommand()
    {
        return _command;
    }
}