﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterTrigger : MonoBehaviour
{
    public CharacterController characterController;
    public Animator animator;
    public Rigidbody2D rb2;
    public float shiftingDist = 1;
    public float speed = 1;
    private bool moveback = false;
    private bool moveforward = false;
    private bool canBeCollided = true;
    private Vector2 target;
    private float step;

    void FixedUpdate()
    {
        if(characterController.isGround)
        {
            canBeCollided = true;
        }
        Moveback();
        Moveforward();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        characterController.SetFalling();
        //can not be collided again before he touches ground
        if (other.CompareTag("Enemy") && canBeCollided)
        {
            step = speed * Time.deltaTime;
            target = new Vector2(transform.position.x - shiftingDist, transform.position.y + shiftingDist);
            moveback = true;
            //avoid the glitch from simulated physical collision
            rb2.simulated = false;
            animator.SetTrigger("RollBack");
        }
        //can always eat another star in the air
        if (other.CompareTag("Star")) 
        {
            step = speed * Time.deltaTime;
            target = new Vector2(transform.position.x + shiftingDist, transform.position.y + shiftingDist);
            moveforward = true;
            //avoid the glitch from simulated physical collision
            rb2.simulated = false;
            animator.SetTrigger("RollForward");
            //eat the star :D
            Destroy(other.gameObject);
        }
        canBeCollided = false;
    }
    void Moveforward()
    {
        if (Mathf.Approximately(transform.position.x, target.x))
        {
            moveforward = false;
            rb2.simulated = true;
        }
        if (moveforward)
        {
            transform.position = Vector2.MoveTowards(transform.position, target, step);
        }
    }
    void Moveback()
    {
        if (Mathf.Approximately(transform.position.x, target.x))
        {
            moveback = false;
            rb2.simulated = true;
        }
        if (moveback)
        {
            transform.position = Vector2.MoveTowards(transform.position, target, step);
        }
    }

}
