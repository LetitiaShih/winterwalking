﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UserInput
{
    public class InputProvider : MonoBehaviour
    {
        public bool tapLeft = false;
        public bool tapRight = false;
        public bool isFingerHold = false;
        public bool isFingerDown = false;

        // TODO: rename these function names
        private void LeftHold()
        {
            tapLeft = true;
            tapRight = false;
            isFingerHold = true;
        }

        private void RightClick()
        {
            tapLeft = false;
            tapRight = true;

            isFingerDown = true;
            isFingerHold = false; 
        }

        private void RightHold()
        {
            tapLeft = false;
            tapRight = true;

            isFingerDown = false;
            isFingerHold = true;
        }

        private void NoAction()
        {
            tapLeft = false;
            tapRight = false;

            isFingerDown = false;
            isFingerHold = false;
        }

        void FixedUpdate()
        {
            if (Input.touchCount > 0)
            {
                ProcessTouchEvent();
            }
            else if (Input.mousePresent)
            {
                ProcessMouseEvent();
            }
            else
            {
                NoAction();
            }
        }
        void ProcessTouchEvent()
        {
            Touch touch = Input.GetTouch(0);
            if (touch.position.x < Screen.width / 2)
            {
                //tap screen left: crouch
                LeftHold();
            }
            else if (touch.position.x >= Screen.width / 2)
            {
                //tap screen right: jump or fly
                if (touch.phase == TouchPhase.Began)
                {
                    RightClick();
                }
                else if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
                {
                    RightHold();
                }
                else if (touch.phase == TouchPhase.Ended)
                {
                    NoAction();
                }
            }
            else
            {
                Debug.Log("Unexpected touch event happened");
            }
        }

        private static int LEFT_MOUSE_BUTTON = 0;
        private static int RIGHT_MOUSE_BUTTON = 1;
        private void ProcessMouseEvent()
        {
            if (Input.GetMouseButton(RIGHT_MOUSE_BUTTON) || Input.GetMouseButtonDown(RIGHT_MOUSE_BUTTON) || Input.GetMouseButtonUp(RIGHT_MOUSE_BUTTON))
            {
                LeftHold();
            }
            else if (Input.GetMouseButtonDown(LEFT_MOUSE_BUTTON))
            {
                RightClick();
            }
            else if (Input.GetMouseButton(LEFT_MOUSE_BUTTON))
            {
                RightHold();
            }
            else
            {
                NoAction();
            }
        }
    }
}
