﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform starSpawnPose;
    public Transform snowballSpawnPose;
    public Transform penguinSpawnPose;
    public GameObject star;
    public GameObject snowball;
    public GameObject penguin;
    public float randomMaxY = 1;

    void Start()
    {
        StartCoroutine(SpawnObject());
    }
    void Update()
    {

    }
    IEnumerator SpawnObject()
    {
        yield return new WaitForSeconds(1);
        while (true)
        {
            switch (Random.Range(0, 3))
            {
                case 0:
                    SpawnStar();
                    break;
                case 1:
                    SpawnPenguin();
                    break;
                case 2:
                    SpawnSnowball();
                    break;
                default:
                    Debug.Log("Unexpected random case");
                    break;
            }
            yield return new WaitForSeconds(Random.Range(1f, 3f));
        }
    }

    void SpawnStar()
    {
        float offsetY = Random.Range(-randomMaxY, randomMaxY);
        Vector2 spawnPosition = new Vector2(starSpawnPose.position.x, starSpawnPose.position.y + offsetY);
        Instantiate(star, spawnPosition, Quaternion.identity);
    }
    void SpawnPenguin()
    {
        float offsetY = Random.Range(-randomMaxY, randomMaxY);
        Vector2 spawnPosition = new Vector2(penguinSpawnPose.position.x, penguinSpawnPose.position.y + offsetY);
        Instantiate(penguin, spawnPosition, Quaternion.identity);
    }
    void SpawnSnowball()
    {
        Instantiate(snowball, snowballSpawnPose.position, Quaternion.identity);
    }
}
