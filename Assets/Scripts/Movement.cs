﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Behaviour{
    public class Movement : MonoBehaviour
    {
        public Collider2D disableCollider;
        public float jumpForceMax;
        public float speedDecreaseFactor;
        private float jumpForce;
        private float stayTime = 0;
        void Start()
        {
            jumpForce = jumpForceMax;
        }
        public void Jump(Rigidbody2D rb2)
        {
            jumpForce = jumpForceMax;
            stayTime = 0;
        }

        public void StayAir(Rigidbody2D rb2)
        {
            if (disableCollider != null)
            {
                disableCollider.enabled = true;
            }
            stayTime += Time.deltaTime;
            int decreaseFactor = Mathf.RoundToInt(stayTime / 0.1f);
            float speed = jumpForce * Mathf.Pow(speedDecreaseFactor, decreaseFactor);
            if (speed < jumpForceMax * 0.3) return;
            rb2.velocity = Vector2.up * speed;
        }
        public void Crouch()
        {
            if (disableCollider != null)
            {
                disableCollider.enabled = false;
            }
        }
        public void Stand()
        {
            if (disableCollider != null)
            {
                disableCollider.enabled = true;
            }
            jumpForce = jumpForceMax;
            stayTime = 0;
        }
    }

}
