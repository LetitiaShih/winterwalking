using UnityEngine;
class MyTestInputProvider : MyInputProvider
{
    public void UpdateInput() { }
    public Command GetCommand()
    {
        return Command.NoAction;
    }
    //can return and command back for testing purpose
}