﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UserInput;
using Behaviour;

public class CharacterController : MonoBehaviour
{
    public Rigidbody2D rb2;
    public Movement movement;
    public InputProvider screenInput;
    public Animator animator;
    public Transform groundCheck;
    public LayerMask groundLayer;

    [Range(0, 0.5f)]
    public float groundDistance;
    private float firstJumpTime;
    private bool canSecondJumped = false;
    public bool isGround
    {
        get
        {
            Vector2 start = groundCheck.position;
            Vector2 end = new Vector2(start.x, start.y - groundDistance);
            Debug.DrawLine(start, end, Color.blue);
            bool grounded = Physics2D.Linecast(start, end, groundLayer);
            return grounded;
        }
    }
    private bool isIncreasingHeigh = false;
    private bool isFalling
    {
        get
        {
            return rb2.velocity.y <= 0;
        }
    }

    //private MyInputProvider myInputProvider;

    // void Start()
    // {
    //     // if (AUTO_TEST_CODE)
    //     // {
    //     //    myInputProvider = new MyTestInputProvider();
    //     //    return;   
    //     // }

    //     if (Input.mousePresent)
    //     {
    //         myInputProvider = new MyMouseInputProvider();
    //     }
    //     else if (Input.touchSupported)
    //     {   
    //         myInputProvider = new MyScreenInputProvider();
    //     }
    //     // ??? Other input deivces?
    //     else 
    //     {
    //         Debug.Log("No Input Device");
    //         // TODO: notify user there is no input devices?
    //     }
    // }

    void FixedUpdate()
    {
        //myInputProvider.UpdateInput();

        float current = Time.time;
        //make the rolling anim can back to walking anim instantly
        if(isGround)
        {
            animator.SetBool("isGround", true);
        }
        else
        {
            animator.SetBool("isGround", false);
        }
        if (isFalling)
        {
            isIncreasingHeigh = false;
        }

        // switch (myInputProvider.GetCommand())
        // {
        //     case Command.Crouch:
        //     {
        //         // TODO:
        //         break;
        //     }
        //     case Command.Jump:
        //     {
        //         // Check if on the ground, if yes, jump.
        //         // Check if in the air and current - firstJumpTime >= 0.1), do second jump
        //         // Otherwise, does nothing.
        //         break;
        //     }
        //     case Command.HoldJump:
        //     {
        //         break;
        //     }
        //     case Command.NoAction:
        //     {
        //         // TODO:
        //         break;
        //     }
        // }

        if (screenInput.tapRight && screenInput.isFingerDown)
        {
            // Try to jump the character.
            if (isGround)
            {
                Debug.Log("Doing First Jump");
                // Jump from ground
                canSecondJumped = true;
                movement.Jump(rb2);
                animator.SetBool("Jump1", true);
                animator.SetBool("Jump2", false);
                movement.StayAir(rb2);
                isIncreasingHeigh = true;
                firstJumpTime = current;

            }
            else if (canSecondJumped && current - firstJumpTime >= 0.1)
            {
                // Second jump in air. Don't allow second jump immediately after first jump
                canSecondJumped = false;
                movement.Jump(rb2);
                animator.SetBool("Jump1", true);
                animator.SetBool("Jump2", true);
                movement.StayAir(rb2);
                isIncreasingHeigh = true;
            }
        }
        else if (isGround)
        {
            // Character hit or stand at ground.
            canSecondJumped = false;
            if (screenInput.tapLeft && screenInput.isFingerHold)
            {
                // Holding crouch button
                movement.Crouch();
                animator.SetBool("Crouch", true);
            }
            else
            {
                // Walking.
                movement.Stand();
                animator.SetBool("Crouch", false);
            }
            animator.SetBool("Jump1", false);
            animator.SetBool("Jump2", false);
        }
        else if (screenInput.tapRight && screenInput.isFingerHold && isIncreasingHeigh && !isFalling)
        {
            // Raise the character.
            // The character has jumpped and user is still holding the button.
            movement.StayAir(rb2);
        }
        else if (!screenInput.isFingerHold)
        {
            // The button is released so the character is falling.
            // After this event, holding character doesn't increase character's heigh.
            isIncreasingHeigh = false;
        }
    }
    public void SetFalling()
    {
        rb2.velocity = new Vector2(0, 0);
    }

}
