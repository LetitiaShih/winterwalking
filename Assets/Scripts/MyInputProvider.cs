interface MyInputProvider
{
    void UpdateInput();
    Command GetCommand();
}