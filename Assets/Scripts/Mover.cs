﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
	public	float speed;
	void Start () {
		Rigidbody2D rb2 = GetComponent<Rigidbody2D>();
		rb2.velocity = -(transform.right) * speed;
		
	}
}
